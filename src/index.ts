// import * from './components/Observer';
// import {
//   ConcreteObserverA,
//   ConcreteObserverB,
//   ConcreteSubject,
// } from './components/Observer';
// import { Singleton2 } from './components/Singleton';

// // const observer = new Observer();

// const subject = new ConcreteSubject();
// const observerA = new ConcreteObserverA();
// subject.attach(observerA);
// const observerB = new ConcreteObserverB();
// subject.attach(observerB);
// subject.someBusinessLogic();
// subject.someBusinessLogic();
// subject.someBusinessLogic();
// subject.attach(observerB);
// subject.someBusinessLogic();
// subject.someBusinessLogic();

// // Singleton

// function clientCode(): void {
//   const s1 = Singleton2.getInstance();
//   const s2 = Singleton2.getInstance();
//   s1.someValue = 123;
//   console.log(s2.someValue);
// }

// clientCode();

// Command

// import * from './components/Command'
import {
  ComplexCommand,
  Invoker,
  Reciever,
  SimpleCommand,
} from './components/Command';

console.log('test3');

const invoker = new Invoker();
invoker.setOnStart(new SimpleCommand('say hi'));

const reciever = new Reciever();
invoker.setOnFinish(new ComplexCommand(reciever, 'send email', 'save report'));

invoker.doSomethingImportant();
