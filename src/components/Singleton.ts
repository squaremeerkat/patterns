export default class Singleton2 {
  private static instance: Singleton2;

  public someValue?: number;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  public static getInstance(): Singleton2 {
    if (!Singleton2.instance) {
      Singleton2.instance = new Singleton2();
    }

    return Singleton2.instance;
  }
}
