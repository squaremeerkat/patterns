/* eslint-disable class-methods-use-this */
/* eslint-disable max-classes-per-file */

interface Command {
  execute(): void;
}

export class SimpleCommand implements Command {
  private payload: string;

  constructor(payload: string) {
    this.payload = payload;
  }

  public execute(): void {
    console.log('do something', this.payload);
  }
}

export class ComplexCommand implements Command {
  private reciever: Reciever;

  private a: string;

  private b: string;

  constructor(reciever: Reciever, a: string, b: string) {
    this.reciever = reciever;
    this.a = a;
    this.b = b;
  }

  public execute(): void {
    console.log('complex command');
    this.reciever.doSomething(this.a);
    this.reciever.doSomethingElse(this.b);
  }
}

export class Reciever {
  doSomething(a: string): void {
    console.log('do', a);
  }

  doSomethingElse(a: string): void {
    console.log('do', a);
  }
}

export class Invoker {
  private onStart?: Command;

  private onFinish?: Command;

  public setOnStart(command: Command) {
    this.onStart = command;
  }

  public setOnFinish(command: Command) {
    this.onFinish = command;
  }

  public doSomethingImportant(): void {
    console.log('do smth important');

    if (this.onStart && this.isCommand(this.onStart)) {
      this.onStart.execute();
    }
    console.log('do smth important2');
    if (this.onFinish && this.isCommand(this.onFinish)) {
      this.onFinish.execute();
    }
  }

  private isCommand(object: Command): object is Command {
    return object.execute !== undefined;
  }
}
