/* eslint-disable max-classes-per-file */
interface Subject {
  state: number;
  attach(observer: Observer): void;
  detach(observer: Observer): void;
  notify(): void;
}

class ConcreteSubject implements Subject {
  public state = 0;

  private observers: Observer[] = [];

  public attach(observer: Observer): void {
    this.observers.push(observer);
    console.log('attached', observer);
  }

  public detach(observer: Observer): void {
    const observerIndex = this.observers.indexOf(observer);
    this.observers.splice(observerIndex, 1);
    console.log('detached', observer);
  }

  public notify(): void {
    console.log('notifying', this.observers);
    this.observers.forEach(observer => {
      observer.update(this);
    });
  }

  public someBusinessLogic(): void {
    this.state = Math.floor(Math.random() * 10 + 1);
    console.log('some logic', this.state);
    this.notify();
  }
}

interface Observer {
  update(subject: Subject): void;
}

class ConcreteObserverA implements Observer {
  // eslint-disable-next-line class-methods-use-this
  update(subject: Subject): void {
    if (subject.state < 3) {
      console.log('A: state < 3', subject.state);
    }
  }
}

class ConcreteObserverB implements Observer {
  // eslint-disable-next-line class-methods-use-this
  update(subject: Subject): void {
    if (subject.state === 0 || subject.state >= 2) {
      console.log('B: state >= 2 || == 0', subject.state);
    }
  }
}

const subject = new ConcreteSubject();
const observerA = new ConcreteObserverA();
subject.attach(observerA);
const observerB = new ConcreteObserverB();
subject.attach(observerB);
subject.someBusinessLogic();
subject.someBusinessLogic();
subject.someBusinessLogic();
subject.attach(observerB);
subject.someBusinessLogic();
subject.someBusinessLogic();
