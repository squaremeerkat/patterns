/* eslint-disable max-classes-per-file */

export class Singl {
  private static instance: Singl;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  static getInstance() {
    if (!Singl.instance) {
      Singl.instance = new Singl();
    }
    return Singl.instance;
  }
}

console.log(Singl.getInstance());

export class Singleton {
  private static instance: Singleton;

  private constructor() {
    // do something construct...
  }

  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton();
      // ... any one time initialization goes here ...
    }
    return Singleton.instance;
  }

  someMethod() {
    console.log('some method', this);
  }
}

// let something = new Singleton() // Error: constructor of 'Singleton' is private.

const instance = Singleton.getInstance(); // do something with the instance...
